class_name FakeState
extends State

var enter_called := false
var exit_called := false
var time_enter_called: int
var time_exit_called: int
var last_msg: Dictionary

signal input_handled(event)
signal updated(delta)
signal updated_physics(delta)

func enter(msg := {}) -> void:
	last_msg = msg
	enter_called = true
	time_enter_called = Time.get_ticks_usec()

func exit() -> void:
	exit_called = true
	time_exit_called = Time.get_ticks_usec()


func handle_input(event: InputEvent) -> void:
	emit_signal("input_handled", event)


func update(delta: float) -> void:
	emit_signal("updated", delta)


func physics_update(delta: float) -> void:
	emit_signal("updated_physics", delta)
