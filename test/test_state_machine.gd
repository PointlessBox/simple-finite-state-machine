class_name TestStateMachine
extends GutTest


var nodes: Array = []


func autofree_state_machine() -> StateMachine:
	return autoqfree(StateMachine.new())


func autofree_state() -> State:
	return autoqfree(FakeState.new())


# Initializes the given `state_machine` with the given `states`
# and attaches it to the scenetrees root. The first state in `states`
# becomes the initial active state.
func init_and_add_state_machine(
	state_machine: StateMachine,
	states: Array
) -> void:
	var node: Node = autoqfree(Node.new())
	nodes.append(node)
	for state in states:
		state_machine.add_child(state as State)
	node.add_child(state_machine)
	state_machine.owner = node
	state_machine.active_state = states[0]
	get_tree().root.add_child(node)


func delayed_transition(
	state_machine: StateMachine,
	target: String,
	delayInSec: float,
	msg := {}
) -> void:
	await get_tree().create_timer(delayInSec).timeout
	state_machine.transition_to(target, msg)


var _sender = InputSender.new(Input)


func after_each():
	var root = get_tree().root
	#for child in nodes:
	#	root.remove_child(child)
	_sender.release_all()
	_sender.clear()


func test_owner_is_ready_when_first_state_is_entered() -> void:
	var fake_state := autofree_state()
	init_and_add_state_machine(autofree_state_machine(), [fake_state])
	assert_true(fake_state.enter_called)


func test_state_machine_injects_iteself_into_its_child_states() -> void:
	var state_machine := autofree_state_machine()
	var fake_state := autofree_state()
	init_and_add_state_machine(state_machine, [fake_state])
	for child in state_machine.get_children():
		assert_eq(child.state_machine, state_machine)


func test_state_machine_delegates_unhandled_input_to_active_state() -> void:
	var state_machine := autofree_state_machine()
	var fake_state := autofree_state()
	init_and_add_state_machine(state_machine, [fake_state])
	_sender.key_down(KEY_SPACE)
	var event = await fake_state.input_handled
	assert_true(event != null)


func test_state_machine_delegates_process_to_active_state() -> void:
	var state_machine := autofree_state_machine()
	var fake_state := autofree_state()
	init_and_add_state_machine(state_machine, [fake_state])
	var delta = await fake_state.updated
	assert_true(delta != 0)


func test_state_machine_delegates_physics_process_to_active_state() -> void:
	var state_machine := autofree_state_machine()
	var fake_state := autofree_state()
	init_and_add_state_machine(state_machine, [fake_state])
	var delta = await fake_state.updated_physics
	assert_true(delta != 0)


func test_state_machine_calls_exit_on_the_current_state_and_then_enters_the_new_state() -> void:
	var state_machine := autofree_state_machine()
	var fake_state_1 := autofree_state()
	var fake_state_2 := autofree_state()
	var state_1_name := "STATE_1"
	var state_2_name := "STATE_2"
	fake_state_1.name = state_1_name
	fake_state_2.name = state_2_name
	init_and_add_state_machine(state_machine, [fake_state_1, fake_state_2])
	state_machine.transition_to(state_2_name)
	assert_true(fake_state_1.time_exit_called < fake_state_2.time_enter_called)


func test_state_machine_does_nothing_when_transitioning_to_non_existing_state() -> void:
	var state_machine := autofree_state_machine()
	var fake_state := autofree_state()
	init_and_add_state_machine(state_machine, [fake_state])
	state_machine.transition_to("DOES NOT EXIST")
	assert_false(fake_state.exit_called)


func test_state_machine_emits_transitioned_signal_when_transition_is_done() -> void:
	var state_machine := autofree_state_machine()
	var fake_state_1 := autofree_state()
	var fake_state_2 := autofree_state()
	var state_1_name := "STATE_1"
	var state_2_name := "STATE_2"
	fake_state_1.name = state_1_name
	fake_state_2.name = state_2_name
	init_and_add_state_machine(state_machine, [fake_state_1, fake_state_2])
	delayed_transition(state_machine, state_2_name, 0.1)
	await state_machine.transitioned
	assert_true(true)


func test_state_machine_emits_the_name_of_the_target_state() -> void:
	var state_machine := autofree_state_machine()
	var fake_state_1 := autofree_state()
	var fake_state_2 := autofree_state()
	var state_1_name := "STATE_1"
	var state_2_name := "STATE_2"
	fake_state_1.name = state_1_name
	fake_state_2.name = state_2_name
	init_and_add_state_machine(state_machine, [fake_state_1, fake_state_2])
	delayed_transition(state_machine, state_2_name, 0.1)
	var next_state_name = await state_machine.transitioned
	assert_eq(next_state_name, state_2_name)


func test_when_state_transitions_then_state_machine_forwards_given_msg_dict() -> void:
	var state_machine := autofree_state_machine()
	var fake_state_1 := autofree_state()
	var fake_state_2 := autofree_state()
	var state_1_name := "STATE_1"
	var state_2_name := "STATE_2"
	fake_state_1.name = state_1_name
	fake_state_2.name = state_2_name
	init_and_add_state_machine(state_machine, [fake_state_1, fake_state_2])
	var msg = { "some_msg": 1 }
	delayed_transition(state_machine, state_2_name, 0.1, msg)
	await state_machine.transitioned
	assert_true(fake_state_2.last_msg != null)
	assert_eq(fake_state_2.last_msg.some_msg, msg.some_msg)
