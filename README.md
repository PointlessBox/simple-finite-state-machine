A simple finite state machine for godot made with gdscript. The state machine and its states are represented by nodes.
It is based on a [tutorial](https://www.gdquest.com/tutorial/godot/design-patterns/finite-state-machine/) by [gdquest.com](https://www.gdquest.com/)
