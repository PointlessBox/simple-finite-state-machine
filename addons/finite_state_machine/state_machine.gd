# Manages the transition and delegation of states.
# To use a [StateMachine] on a [Node] just add [StateMachine] as a child node.
# To add states to the [StateMachine] extend the [State] class, and add your states as child nodes to the [StateMachine] node.
class_name StateMachine
extends Node

# Emitted when the transition to a new state is done successfully.
# Contains the `Node.name` of the target node of the transition.
signal transitioned(state_name)

# Points to the node representing the inital active_state. Exported so it is editable in the godot gui-editor.
@export var initial_state := NodePath()
@export var initial_msg := Dictionary()

var active_state: State


func _ready() -> void:
	# self.active_state is not an onready var, so it is possible
	# to set it directly for testing puposes.
	if active_state == null:
		active_state = get_node(initial_state)
	# Await for the owner node to be ready
	# so we can safely use the `owner` variable
	await owner.ready
	# Setting the `state_machine` variable in 
	# the State-instances so they can call
	# `transition_to` on the StateMachine-instance.
	for child in get_children():
		child.state_machine = self
	active_state.enter(initial_msg)


func _unhandled_input(event) -> void:
	active_state.handle_input(event)


func _process(delta: float) -> void:
	active_state.update(delta)


func _physics_process(delta: float) -> void:
	active_state.physics_update(delta)


# Called by [State] instances to initiate a state transition.
# Returns early if the given [target_state_name] is not a child of this [StateMachine] instance.
func transition_to(target_state_name: String, msg: Dictionary = {}) -> void:
	msg.merge(initial_msg, true)
	if not has_node(target_state_name):
		return
	
	active_state.exit()
	active_state = get_node(target_state_name)
	active_state.enter(msg)
	emit_signal("transitioned", active_state.name)
