@tool
extends EditorPlugin

const STATE_MACHINE_NODE := "StateMachine"

func _enter_tree():
	add_custom_type(STATE_MACHINE_NODE, "Node", preload("state_machine.gd"), preload("res://addons/finite_state_machine/icon.png"))
	pass


func _exit_tree():
	remove_custom_type(STATE_MACHINE_NODE)
	pass
