# Abstract class. Extend it to create own states.
# A [State] can be attached to a [StateMachine] by adding it as a child node to the [StateMachine] node.
class_name State
extends Node


# Reference to the state machine. Use inside state classes to
# transition between states.
var state_machine: StateMachine = null


# Used by [StateMachine] to delegate [_unhandled_input()] to this state.
func handle_input(_event: InputEvent) -> void:
	pass


# Used by [StateMachine] to delegate [_process()] to this state.
func update(_delta: float) -> void:
	pass


# Used by [StateMachine] to delegate [_physics_process()] to this state.
func physics_update(_delta: float) -> void:
	pass


# Used by [StateMachine] to enter this state.
func enter(_msg := {}) -> void:
	pass


# Used by [StateMachine] to exit this state.
func exit() -> void:
	pass
